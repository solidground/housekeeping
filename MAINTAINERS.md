# Solidground maintainers

Solidground Project is maintained by the following people:

- Aravinth Manivannan, [@realaravinth](https://codeberg.org/realaravinth), [fediverse](https://gts.batsense.net/@realaravinth), [matrix](https://matrix.to/#/@realaravinth:matrix.batsense.net)

- Arnold Schrijver, [@circlebuilder](https://codeberg.org/circlebuilder), [fediverse](https://social.coop/@smallcircles), [matrix](https://matrix.to/#/@circlebuilder:matrix.org)
