<div align="center">

[![solidground logo](assets/images/solidground-logo-white-500x103.png)](https://solidground.work)

### [Website](https://solidground.work) | [Docs](https://docs.solidground.work) | [Chat](https://matrix.to/#/#solidground-matters:matrix.org) | [Forum](https://discuss.coding.social/c/solidground/13)

</div>

<br>

# Welcome to Housekeeping

Here you can suggest improvements to our development proces and find the places where we are active. Read the [Contribution Guidelines](CONTRIBUTING.md) before suggesting changes.

## Contents

- [Community](#community)
- [Roadmap](#roadmap)
- [Participate](#participate)
- [License](#license)


## Community

Join us and discuss on Solidground's roadmap and features. We get together at these places:

- [#solidground:matrix.org](https://matrix.to/#/#solidground:matrix.org): Chatroom for general discussion.
- [#groundwork-matters:matrix.org](https://matrix.to/#/#groundwork-matters:matrix.org): Chatroom for technical matters and ongoing activity.
- [Solidground forum](https://discuss.coding.social/c/solidground/13): Several categories exist for long-form discussion.
- [Fedi foundation](https://fedi.foundation): We blog at this collaborative e-zine for the Fediverse.
- [Community meetups](https://meet.waag.org/LayGroundworkStandOnSolidground): We use Jitsi provided by [waag.org](https://waag.org)

Other tools we use are:

- [Penpot.app](https://design.penpot.app/#/dashboard/team/0a3a6230-3cbc-11ed-a970-cf2300df1f4f/projects): Solidground workspace for UI/UX designs.
- [Diagrams.net](https://diagrams.net): For technical diagrams (note: include diagram definition with your image export).
- [Inkscape](https://inkscape.org): For SVG diagrams and artwork such as logo's.
- [LibreOffice](https://libreoffice.org): When we need to prepare documents for some reason.

## Roadmap

_(TODO [#43](https://codeberg.org/solidground/housekeeping/issues/43))_

## Participate

We practice [Social Coding Principles](https://coding.social/principles). By participating in our
projects you consent to our [Community Participation Guidelines](CODE_OF_CONDUCT.md).

## License

Copyright (c) 2022-present [Solidground](https://solidground.work) and contributors.

Content in this repository is licensed under [Creative Commons Attribution-ShareAlike 4.0 International](LICENSE).

