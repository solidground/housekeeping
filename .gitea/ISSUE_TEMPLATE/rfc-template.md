---
name: "Submit a RFC"
about: "Propose a change to architecture, techstack or project direction."
title: "[RFC] "
ref: "main"
labels:
- RFC
---

# RFC: (Title of your proposal)

<!--
  This issue template was derived from the wasmCloud project, licensed Apache 2.0.
  https://github.com/wasmCloud/wasmCloud/blob/main/.github/ISSUE_TEMPLATE/rfc-template.md
-->

## Introduction

(Here, provide a brief summary of the feature or idea that you're proposing. This should be a couple of sentences or a short paragraph.)

## Motivation

(Explain the background behind this RFC. What problem are you trying to solve? Why is this solution needed? Why is it relevant for your project? This is your chance to provide a persuasive argument for your proposal.)

## Detailed Design

(This is the heart of your RFC. Describe your proposed solution in detail. If you're suggesting a new feature, how will it work? If it's a new policy or process, what are the steps involved? Be as specific as possible. Include any code samples, diagrams, or other materials that would help others understand your proposal.)

## Backwards Compatibility

(Discuss how your proposal will affect existing parts of your project. Will it introduce breaking changes? How will you mitigate these issues?)

## Alternatives Considered

(Describe any alternative solutions that you considered and why you chose your current proposal over them. This demonstrates that you've thought carefully about your proposal and helps others understand your decision-making process.)

## Unresolved Questions

(List any questions or issues that you weren't able to address in your proposal. This helps others understand what feedback you're looking for.)

## Conclusion

(End with a summary of your proposal and the next steps. What kind of feedback are you looking for? When do you hope to make a decision? Be clear about the process moving forward.)

---

- When this RFC is `adr-accepted` an ADR document MUST be added to [Architecture Decisions](https://docs.soldiground.work/adrs/)
  - See the [RFC to ADR Process](https://docs.soldiground.work/adrs/0001/) and [ADR Styleguide](https://docs.soldiground.work/adrs/0002/).