# Welcome to Solidground Project

We are very pleased that you are investing your time in contributing to our project. Social coding is about people and we welcome you to join us where you see fit. Read our [Code of Conduct](CODE_OF_CONDUCT.md) to learn how we strive to keep our community approachable and inclusive to anyone's participation.

## Contents

- [New contributor](#new-contributor)
- [Getting started](#getting-started)
- [Tools we use](#tools-we-use)

## New contributor

The [README](README.md) of this repository list the channels where our community is active. We value contributions of any kind, be they code, documentation, designs or tests. And above all we like to hear from you. Come into our chat or post to the forum and share your thoughts and ideas on the future of Solidground Project. Get to know other members that are active in our community.

## Getting started

Here in Housekeeping repository is where you find general information on the work we do. The code repositories in the Solidground organization each have their own `CONTRIBUTING.md` with specifics for the particular codebase.

Housekeeping involves improving our development processes and documenting procedures so we can be more effective as a community. In this repository the [Issue Tracker](https://codeberg.org/solidground/housekeeping/issues) is where suggestions for improvement can be made.

### Before creating an issue

Before you create a new issue in the tracker it is best to first check in on the community chat and discuss your proposal. The [#solidground](https://matrix.to/#/#solidground:matrix.org) Matrix chatroom is your best starting point. Also check existing issue to see if something similar doesn't already exist, and it is better to add a comment instead.

### Create a new issue

First [Create a New Issue](https://codeberg.org/solidground/housekeeping/issues/new).

Describe the changes you want to see. Try to be concise yet complete in your description, and provide references to external resources where relevant. It can be very useful for example to include a link to the chat comment on Matrix where you discussed matters before. Also mention other community members if they were involved, or you want their attention.

Check the [Labels](https://codeberg.org/solidground/housekeeping/labels) that are available and add those that help categorize your issue. If you are willing and able to realize the proposal you make, then Assign yourself to the issue.

### After issue creation

After saving the issue consider posting the link in the relevant chat channel or in a post to the discussion forum. Sometimes it can take long before your issue gets a response. If that happens it is best to first bring it up in the chat. If the issue received comments but no further follow-up you can add a comment with a gentle reminder.

## Tools we use

In Solidground Project we highly value [Free and Open Source Software](https://en.wikipedia.org/wiki/Free_and_open-source_software) (FOSS), and we carefully selected our community tools accordingly. Take this in consideration and discuss on the chat before you use tools of your own, in particular when they are not FOSS.